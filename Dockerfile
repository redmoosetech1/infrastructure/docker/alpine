FROM alpine:3.15.0
LABEL version="3.15.0"
LABEL maintainer="RedMooseTech nate@redmoose.tech"
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.name="RMT Alpine"
LABEL org.label-schema.description="RMT Base Alpine Image"
LABEL org.label-schema.vcs-url="https://gitlab.rmt/infrastructure/docker/alpine"
LABEL org.label-schema.vendor="RedMooseTech"
LABEL org.label-schema.version=3.15.0
LABEL org.label-schema.schema-version="1.0"
LABEL com.microscaling.docker.dockerfile="/Dockerfile"

# Configure Internal Mirror
RUN /bin/sed -i 's/https:\/\/dl-cdn.alpinelinux.org/http:\/\/mirror.rmt/g' /etc/apk/repositories
RUN apk add --update --no-cache \
    ca-certificates \
    bash \
    && rm -rf /var/cache/apk/*

ADD https://mirror.rmt/certificates/rmt.crt /usr/local/share/ca-certificates/rmt.crt
RUN update-ca-certificates
